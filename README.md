<div align="center">
    <h1>AC Academy - DevOps Essentials</h1>
</div>

# :question: About
This project will be used as part of the DevOps course powered by AC Academy, it implements a simple endpoint that will be exposed as a REST API.

The objective here is to put in practice DevOps skills learned during the course.

# :rocket: Getting started
## :whale: Running in Docker
```
$ ./mvnw clean package
$ docker-compose build
$ docker-compose up -d
$ docker-compose logs -f ac-academy-devops-api
```

### :white_check_mark: Testing
After building and running the container successfully, you might want to test it. To do so, send a GET request to the following URL: `http://localhost:8080/test`.

You should receive a response payload like this:
```
{
    "timestamp": "2021-06-26T23:39:24.825525",
    "message": "Hello World!",
    "status": "OK"
}
```

## :clipboard: Wait-for-it
As you can see, there's a bash script `wait-for-it.sh` in the project's directory. This script will listen to the MySQL datasource container at a specific port, and make sure the database container is up and running before it continues with the API container, this will prevent the application from failing due to datasource connection errors, considering that sometimes the API container starts running before the DB is fully initialized (even with a `depends_on` property set up in the docker-compose.yml), resulting in a connection failure.

## :inbox_tray: Pushing new images to DockerHub
```
$ ./mvnw clean package
$ docker image build -t gabrielguarido/ac-academy-devops-api .
$ docker push gabrielguarido/ac-academy-devops-api:latest
```

# :computer: Technologies
- Java 11
- Docker
- MySQL
- Spring Framework

# :memo: License
This project is under MIT License, see [LICENSE](LICENSE) file for more information.
