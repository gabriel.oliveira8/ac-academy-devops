package com.avenue.code.academy.devops.util;

import com.avenue.code.academy.devops.model.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class TestUtil {

    private TestUtil() {
    }

    private static final String API_RESPONSE_MESSAGE = "Hello World!";

    public static ApiResponse buildApiResponse() {
        return new ApiResponse(LocalDateTime.now(), API_RESPONSE_MESSAGE, HttpStatus.OK);
    }
}
