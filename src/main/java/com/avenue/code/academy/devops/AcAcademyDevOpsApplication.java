package com.avenue.code.academy.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcAcademyDevOpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcAcademyDevOpsApplication.class, args);
	}

}
