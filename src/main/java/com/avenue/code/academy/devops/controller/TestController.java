package com.avenue.code.academy.devops.controller;

import com.avenue.code.academy.devops.model.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.avenue.code.academy.devops.util.TestUtil.buildApiResponse;

@RestController
@RequestMapping(value = "test", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestController {

    @GetMapping
    public ResponseEntity<ApiResponse> test() {
        return ResponseEntity.ok(buildApiResponse());
    }
}
